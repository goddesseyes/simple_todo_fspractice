from django.shortcuts import render, redirect
from django.views import View
from task.models import Task


class TaskView(View):
    @staticmethod
    def get(request: object) -> str:
        tasks = Task.objects.order_by('-level')
        return render(
            request=request,
            template_name='index.html',
            context={
                'tasks': tasks
            }
        )


class TaskCompleteView(View):
    @staticmethod
    def get(request: object) -> str:
        task = Task.objects.get(
            id=request.GET.get('task_id')
        )
        task.is_todo = True
        task.save()
        return redirect('/')


class TaskDeleteView(View):
    @staticmethod
    def get(request: object) -> str:
        task_id = request.GET.get('task_id')
        task = Task.objects.get(
            id=task_id
        )
        task.delete()
        return redirect('/')
