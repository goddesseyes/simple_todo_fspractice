from django.db import models


# Базовая абстрактная модель для добавления времени создания, изменения во все унаследованные от неё модели.
class BaseModel(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True
