from task.common import *


class Task(BaseModel):
    title = models.CharField(
        max_length=100,
        default='Заголовок задачи!',
        verbose_name='Заголовок задачи',
        blank=False,
        null=False,
    )
    description = models.TextField(
        default='Описание задачи!',
        verbose_name='Описание задачи'
    )
    tag = models.CharField(
        max_length=100,
        verbose_name='Тэг для задачи'
    )
    level = models.PositiveSmallIntegerField(
        default=0,
        unique=False,
    )
    is_todo = models.BooleanField(
        default=False
    )

    def __str__(self):
        return f'{self.title}'

    class Meta:
        verbose_name = 'Задача'
        verbose_name_plural = 'Задачи'
