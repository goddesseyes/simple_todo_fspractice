
from django.contrib import admin
from django.urls import path
from django.conf.urls.static import static
from django.conf import settings
from task.views import TaskView, TaskCompleteView, TaskDeleteView

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', TaskView.as_view(), name='index'),
    path('task_complete/', TaskCompleteView.as_view(), name='task_complete'),
    path('task_delete/', TaskDeleteView.as_view(), name='task_delete'),
]
if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
